package com.example.firebaseexample.Model;

public class Items {
    private int item_id;
    private String name;
    private int price;
    private String image;
    private int store_id;
    private String serial_number;
    private int gender_category_id;
    private int type_category_id;
    private int shoes_or_clothes_id;
    private int fabric_id;

    public Items() {
    }

    public Items(int item_id, String name, int price, String image, int store_id, String serial_number, int gender_category_id, int type_category_id, int shoes_or_clothes_id, int fabric_id) {
        this.item_id = item_id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.store_id = store_id;
        this.serial_number = serial_number;
        this.gender_category_id = gender_category_id;
        this.type_category_id = type_category_id;
        this.shoes_or_clothes_id = shoes_or_clothes_id;
        this.fabric_id = fabric_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }

    public int getGender_category_id() {
        return gender_category_id;
    }

    public void setGender_category_id(int gender_category_id) {
        this.gender_category_id = gender_category_id;
    }

    public int getType_category_id() {
        return type_category_id;
    }

    public void setType_category_id(int type_category_id) {
        this.type_category_id = type_category_id;
    }

    public int getShoes_or_clothes_id() {
        return shoes_or_clothes_id;
    }

    public void setShoes_or_clothes_id(int shoes_or_clothes_id) {
        this.shoes_or_clothes_id = shoes_or_clothes_id;
    }

    public int getFabric_id() {
        return fabric_id;
    }

    public void setFabric_id(int fabric_id) {
        this.fabric_id = fabric_id;
    }
}
