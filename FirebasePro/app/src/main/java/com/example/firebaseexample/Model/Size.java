package com.example.firebaseexample.Model;

import java.io.Serializable;

public class Size implements Serializable {
    private boolean small;
    private boolean medium;
    private boolean large;
    private boolean xLarge;
    private boolean xxLarge;
    private boolean xxxLarge;

    public Size(  boolean small, boolean medium, boolean large, boolean xLarge, boolean xxLarge, boolean xxxLarge) {
        this.small = small;
        this.medium = medium;
        this.large = large;
        this.xLarge = xLarge;
        this.xxLarge = xxLarge;
        this.xxxLarge = xxxLarge;

    }

    public Size() {
    }

    public boolean isSmall() {
        return small;
    }

    public void setSmall(boolean small) {
        this.small = small;
    }

    public boolean isMedium() {
        return medium;
    }

    public void setMedium(boolean medium) {
        this.medium = medium;
    }

    public boolean isLarge() {
        return large;
    }

    public void setLarge(boolean large) {
        this.large = large;
    }

    public boolean isxLarge() {
        return xLarge;
    }

    public void setxLarge(boolean xLarge) {
        this.xLarge = xLarge;
    }

    public boolean isXxLarge() {
        return xxLarge;
    }

    public void setXxLarge(boolean xxLarge) {
        this.xxLarge = xxLarge;
    }

    public boolean isXxxLarge() {
        return xxxLarge;
    }

    public void setXxxLarge(boolean xxxLarge) {
        this.xxxLarge = xxxLarge;
    }


}
