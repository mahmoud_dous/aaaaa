package com.example.firebaseexample.Model;


import java.io.Serializable;

public class MyConstants {

    public MyConstants() {
    }


    public enum myColor {red, black, blue, magenta, gray, yellow, green, white}

    public enum mySize {small, medium, large, xLarge, xxLarge, xxxLarge}

};
