package com.example.firebaseexample.Model;

public class MyColors {
    private String red;
    private String black;
    private String blue;
    private String magenta;
    private String gray;
    private String yellow;
    private String green;
    private String white;

    public MyColors(String red, String black, String blue, String magenta, String gray, String yellow, String green, String white) {
        this.red = red;
        this.black = black;
        this.blue = blue;
        this.magenta = magenta;
        this.gray = gray;
        this.yellow = yellow;
        this.green = green;
        this.white = white;
    }
}
