package com.example.firebaseexample.CustomAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.firebaseexample.Model.TypeClothes;
import com.example.firebaseexample.R;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {

    Context activity;
    ArrayList<TypeClothes> list;
    LayoutInflater inflater;

    public CustomAdapter(Context activity, ArrayList<TypeClothes> list) {
        this.activity = activity;
        this.list = list;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View root = view;
        if (view == null)
            root = inflater.inflate(R.layout.row_spiner, null);


        final TextView name = root.findViewById(R.id.name_type);
        final ImageView imge = root.findViewById(R.id.img_type);


        name.setText(list.get(i).getName());
        imge.setImageResource(list.get(i).getImge());


        return root;
    }
}