package com.example.firebaseexample;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class MyFirebase <T> {

    public DatabaseReference add(DatabaseReference reference, String objectName, T obj){
        DatabaseReference employeeReference = reference.child(objectName);
        employeeReference.setValue(obj);
        return employeeReference;
    }
    public DatabaseReference add2(DatabaseReference reference, String objectName, T obj){
        DatabaseReference employeeReference = reference.child(objectName);
        employeeReference.push().setValue(obj);
        return employeeReference;
    }

    public DatabaseReference addWithPush(DatabaseReference reference, String objectName, T obj){
        DatabaseReference employeeReference = reference.child(objectName);
        employeeReference.push().setValue(obj);
        return employeeReference;
    }
    public static void  getData( Query q , final FirebaseListener firebaseListener) {
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                firebaseListener.onDataChange(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                firebaseListener.onCancelled(databaseError);
            }
        });
    }

    public static void getDataWithRef(DatabaseReference reference, final FirebaseListener firebaseListener){
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                firebaseListener.onDataChange(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                firebaseListener.onCancelled(databaseError);
            }
        });

    }

    public interface FirebaseListener {

         void onDataChange(@NonNull DataSnapshot dataSnapshot);
         void onCancelled(@NonNull DatabaseError databaseError);
    };
}
