package com.example.firebaseexample.Model;

import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;

public class MyItems {
    private MyConstants.myColor color;
    private MyConstants.mySize size;
    private int quantity;
    private String image;
    private String gender;
    private String clothes_type;
    private long date;
    private int price;

    public MyItems(MyConstants.myColor color, MyConstants.mySize size, int quantity, String image,long date) {
        this.color = color;
        this.size = size;
        this.quantity = quantity;
        this.image = image;
        this.date = date;
    }

    public MyItems(MyConstants.myColor color, MyConstants.mySize size, String image, String gender, String clothes_type, long date,int price) {
        this.color = color;
        this.size = size;
        this.image = image;
        this.gender = gender;
        this.clothes_type = clothes_type;
        this.date = date;
        this.price = price;
    }

    public MyItems(MyConstants.myColor color) {
        this.color = color;
    }

    public MyItems() {
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public MyConstants.myColor getColor() {
        return color;
    }

    public void setColor(MyConstants.myColor color) {
        this.color = color;
    }

    public MyConstants.mySize getSize() {
        return size;
    }

    public void setSize(MyConstants.mySize size) {
        this.size = size;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClothes_type() {
        return clothes_type;
    }

    public void setClothes_type(String clothes_type) {
        this.clothes_type = clothes_type;
    }

    @Override
    public String toString() {
        return "MyItems{" +
                "color=" + color +
                ", size=" + size +
                ", quantity=" + quantity +
                ", image='" + image + '\'' +
                ", gender='" + gender + '\'' +
                ", clothes_type='" + clothes_type + '\'' +
                ", date=" + date +
                '}';
    }

    public void store(DatabaseReference stdReference) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("color", color);
        hashMap.put("size", size);
        hashMap.put("quantity", quantity);
        hashMap.put("image", image);
        hashMap.put("date", date);
        hashMap.put("clothes_type", clothes_type);
        hashMap.put("gender", gender);

        DatabaseReference reference = stdReference.child("Items").push();
        reference.setValue(hashMap);

    }
}
