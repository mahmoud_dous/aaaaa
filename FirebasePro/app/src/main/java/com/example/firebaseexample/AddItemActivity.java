package com.example.firebaseexample;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firebaseexample.CustomAdapter.CustomAdapter;
import com.example.firebaseexample.Model.MyConstants;
import com.example.firebaseexample.Model.MyItems;
import com.example.firebaseexample.Model.TypeClothes;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddItemActivity extends AppCompatActivity implements CheckBox.OnCheckedChangeListener {

    CheckBox cbRed, cbBlack, cbBlue, cbMagenta, cbGray, cbYellow, cbGreen, cbWhite;
    LinearLayout containerRed, containerYellow, containerBlue, containerGreen, containerMagenta, containerWhite,
            containerBlack, containerGray, mainCont;
    int quantitySmall, quantityMedium, quantityLarge, quantity_xLarge, quantity_xxLarge, quantity_xxxLarge;
    Button btnContinue;
    boolean small, medium, large, xLarge, xxLarge, xxxLarge;
    MyConstants.myColor color;
    CircleImageView imageView;
    Map<MyConstants.myColor, View> color_views = new HashMap<>();
    ArrayList<TypeClothes> list_clothes = new ArrayList<>();
    ArrayList<TypeClothes> list_gender = new ArrayList<>();
    Spinner sp_clothes,sp_gender;
    String  itemGender,itemClothes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        findViewById();
        checkedChangeListener();
        setColor();

        //  1-Type Clothes

        list_clothes.add(new TypeClothes(1,"جاكيت",R.drawable.jacket));
        list_clothes.add(new TypeClothes(2,"بنطلون",R.drawable.accesories));
        list_clothes.add(new TypeClothes(3,"قميص",R.drawable.shirt));
        list_clothes.add(new TypeClothes(4,"شورت",R.drawable.shortt));
        list_clothes.add(new TypeClothes(5,"تنورة",R.drawable.tnora));
        list_clothes.add(new TypeClothes(6,"فستان",R.drawable.fostan));
        list_clothes.add(new TypeClothes(7,"بلوزة",R.drawable.blouse));

        CustomAdapter customAdapterClothes = new CustomAdapter(getApplicationContext(), list_clothes);
        sp_clothes.setAdapter(customAdapterClothes);

        sp_clothes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                  TypeClothes typeClothes = (TypeClothes) parent.getItemAtPosition(position);
                itemClothes = typeClothes.getName();
                Log.e("item",itemClothes);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // 2- Gender
        list_gender.add(new TypeClothes(1,"رجال",R.drawable.men));
        list_gender.add(new TypeClothes(2,"نساء",R.drawable.women));
        list_gender.add(new TypeClothes(3,"أطفال",R.drawable.baby));

        CustomAdapter customAdapterGender = new CustomAdapter(getApplicationContext(), list_gender);
        sp_gender.setAdapter(customAdapterGender);
        sp_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TypeClothes typeClothes = (TypeClothes) parent.getItemAtPosition(position);
                itemGender = typeClothes.getName();
                Log.e("item",itemGender);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }

    @Override
    public void onCheckedChanged(final CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cbBlack:
                if (isChecked) {

                    addSizeView(containerBlack, "Size allowance of Black Color", MyConstants.myColor.black, R.layout.item_black);
                    containerBlack.setVisibility(View.VISIBLE);


                } else {
                    containerBlack.setVisibility(View.GONE);
                    removeSizeView(containerBlack);
                }
                break;
            case R.id.cbBlue:
                if (isChecked) {
                    addSizeView(containerBlue, "Size allowance of Blue Color", MyConstants.myColor.blue, R.layout.item_blue);
                    containerBlue.setVisibility(View.VISIBLE);


                } else {
                    containerBlue.setVisibility(View.GONE);
                    removeSizeView(containerBlue);
                }
                break;
            case R.id.cbGray:
                if (isChecked) {
                    addSizeView(containerGray, "Size allowance of Gray Color", MyConstants.myColor.gray, R.layout.item_gray);
                    containerGray.setVisibility(View.VISIBLE);

                } else {
                    containerGray.setVisibility(View.GONE);
                    removeSizeView(containerGray);
                }
                break;
            case R.id.cbGreen:
                if (isChecked) {
                    addSizeView(containerGreen, "Size allowance of Green Color", MyConstants.myColor.green, R.layout.item_green);
                    containerGreen.setVisibility(View.VISIBLE);

                } else {
                    containerGreen.setVisibility(View.GONE);
                    removeSizeView(containerGreen);
                }
                break;

            case R.id.cbMagenta:
                if (isChecked) {
                    addSizeView(containerMagenta, "Size allowance of Magenta Color", MyConstants.myColor.magenta, R.layout.item_magenta);
                    containerMagenta.setVisibility(View.VISIBLE);

                } else {
                    containerMagenta.setVisibility(View.GONE);
                    removeSizeView(containerMagenta);
                }
                break;

            case R.id.cbRed:
                if (isChecked) {
                    addSizeView(containerRed, "Size allowance of Red Color", MyConstants.myColor.red, R.layout.item_red);
                    containerRed.setVisibility(View.VISIBLE);

                } else {
                    containerRed.setVisibility(View.GONE);
                    removeSizeView(containerRed);
                }
                break;

            case R.id.cbWhite:
                if (isChecked) {
                    addSizeView(containerWhite, "Size allowance of White Color", MyConstants.myColor.white, R.layout.item_white);
                    containerWhite.setVisibility(View.VISIBLE);

                } else {
                    containerWhite.setVisibility(View.GONE);
                    removeSizeView(containerWhite);
                }
                break;
            case R.id.cbYellow:
                if (isChecked) {
                    addSizeView(containerYellow, "Size allowance of Yellow Color", MyConstants.myColor.yellow, R.layout.item_yellow);
                    containerYellow.setVisibility(View.VISIBLE);

                } else {
                    containerYellow.setVisibility(View.GONE);
                    removeSizeView(containerYellow);
                }
                break;

        }
    }

    public void setColor() {
        cbYellow.setBackground(setGradientColors(Color.YELLOW, Color.YELLOW));
        cbRed.setBackground(setGradientColors(Color.RED, Color.RED));
        cbBlack.setBackground(setGradientColors(Color.BLACK, Color.BLACK));
        cbBlue.setBackground(setGradientColors(Color.BLUE, Color.BLUE));
        cbMagenta.setBackground(setGradientColors(Color.MAGENTA, Color.MAGENTA));
        cbGray.setBackground(setGradientColors(Color.GRAY, Color.GRAY));
        cbGreen.setBackground(setGradientColors(Color.GREEN, Color.GREEN));
        cbWhite.setBackground(setGradientColors(Color.WHITE, Color.WHITE));
    }

    private Drawable setGradientColors(int bottomColor, int topColor) {
        GradientDrawable gradient = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP,
                new int[]{bottomColor, topColor});
        gradient.setShape(GradientDrawable.OVAL);
        gradient.setCornerRadius(10.f);
        return gradient;
    }


    public void addSizeView(LinearLayout linearLayout, String text, final MyConstants.myColor colors, int item) {

        View view = LayoutInflater.from(getApplicationContext()).inflate(item, mainCont, false);
        color_views.put(colors, view);

        TextView tvSize = view.findViewById(R.id.tvSize);
        tvSize.setText(text);

        CheckBox cbSmall = view.findViewById(R.id.cbSmall);
        CheckBox cbMedium = view.findViewById(R.id.cbMedium);
        CheckBox cbLarge = view.findViewById(R.id.cbLarge);
        CheckBox cb_x_large = view.findViewById(R.id.cb_x_large);
        CheckBox cb_xx_large = view.findViewById(R.id.cb_xx_large);
        CheckBox cb_xxx_large = view.findViewById(R.id.cb_xxx_large);
        EditText etPriceOffer = view.findViewById(R.id.etPriceOffer);
        final EditText etPrice = view.findViewById(R.id.etPrice);


        imageView = view.findViewById(R.id.imgProfile);
        imageView.setVisibility(View.VISIBLE);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(gallery, 200);

            }
        });

        cbSmall.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                small = isChecked;
            }
        });

        cbMedium.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                medium = isChecked;
            }
        });

        cbLarge.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                large = isChecked;
            }
        });

        cb_x_large.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                xLarge = isChecked;
            }
        });

        cb_xx_large.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                xxLarge = isChecked;
            }
        });

        cb_xxx_large.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                xxxLarge = isChecked;
            }
        });
        linearLayout.addView(view);

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cbBlack.isChecked()) {
                    color = MyConstants.myColor.black;
                    String strPrice = etPrice.getText().toString();
                    final int price = Integer.parseInt(strPrice);
                    handleColorOptions(color_views.get(color), color, imageView,price);

                }

                if (cbBlue.isChecked()) {
                    color = MyConstants.myColor.blue;
                    String strPrice = etPrice.getText().toString();
                    final int price = Integer.parseInt(strPrice);
                    handleColorOptions(color_views.get(color), color, imageView,price);

                }
                if (cbRed.isChecked()) {
                    color = MyConstants.myColor.red;
                    String strPrice = etPrice.getText().toString();
                    final int price = Integer.parseInt(strPrice);
                    handleColorOptions(color_views.get(color), color, imageView,price);

                }
                if (cbGray.isChecked()) {
                    color = MyConstants.myColor.gray;
                    String strPrice = etPrice.getText().toString();
                    final int price = Integer.parseInt(strPrice);
                    handleColorOptions(color_views.get(color), color, imageView,price);
                }
                if (cbYellow.isChecked()) {
                    color = MyConstants.myColor.yellow;
                    String strPrice = etPrice.getText().toString();
                    final int price = Integer.parseInt(strPrice);
                    handleColorOptions(color_views.get(color), color, imageView,price);
                }
                if (cbGreen.isChecked()) {
                    color = MyConstants.myColor.green;
                    String strPrice = etPrice.getText().toString();
                    final int price = Integer.parseInt(strPrice);
                    handleColorOptions(color_views.get(color), color, imageView,price);
                }
                if (cbMagenta.isChecked()) {
                    color = MyConstants.myColor.magenta;
                    String strPrice = etPrice.getText().toString();
                    final int price = Integer.parseInt(strPrice);
                    handleColorOptions(color_views.get(color), color, imageView,price);
                }

                if (cbWhite.isChecked()) {
                    color = MyConstants.myColor.white;
                    String strPrice = etPrice.getText().toString();
                    final int price = Integer.parseInt(strPrice);
                    handleColorOptions(color_views.get(color), color, imageView,price);
                }


            }
        });
    }

    private void handleColorOptions(View view, final MyConstants.myColor colors, CircleImageView imageView, final int price) {
        final CheckBox cbSmall = view.findViewById(R.id.cbSmall);
        final CheckBox cbMedium = view.findViewById(R.id.cbMedium);
        final CheckBox cbLarge = view.findViewById(R.id.cbLarge);
        final CheckBox cb_x_large = view.findViewById(R.id.cb_x_large);
        final CheckBox cb_xx_large = view.findViewById(R.id.cb_xx_large);
        final CheckBox cb_xxx_large = view.findViewById(R.id.cb_xxx_large);

        imageView = view.findViewById(R.id.imgProfile);

        final ArrayList<MyItems> data = new ArrayList<>();


        //store image in firebase and store items
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference reference = database.getReference();

        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, arrayOutputStream);
        final Date date = new Date();
        final long dateTime = date.getTime();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference refrenceSTR = storage.getReference().child("image").child(dateTime + ".jpg");

        refrenceSTR.putBytes(arrayOutputStream.toByteArray()).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(getApplicationContext(), "upload success", Toast.LENGTH_SHORT).show();

                refrenceSTR.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {

                        String url = uri.toString();
                        if (cbSmall.isChecked()) {
                            MyItems items = new MyItems(colors, MyConstants.mySize.small, url, itemGender,itemClothes,dateTime,price);
                            items.store(reference);

                        }

                        if (cbMedium.isChecked()) {
                            MyItems items = new MyItems(colors, MyConstants.mySize.medium, url,itemGender,itemClothes, dateTime,price);
                            items.store(reference);
                        }

                        if (cbLarge.isChecked() ) {
                            MyItems items = new MyItems(colors, MyConstants.mySize.large, url,itemGender,itemClothes,  dateTime,price);
                            items.store(reference);
                        }

                        if (cb_x_large.isChecked() ) {
                            MyItems items = new MyItems(colors, MyConstants.mySize.xLarge, url,itemGender,itemClothes,  dateTime,price);
                            items.store(reference);
                        }

                        if (cb_xx_large.isChecked()) {
                            MyItems items = new MyItems(colors, MyConstants.mySize.xxLarge, url,itemGender,itemClothes, dateTime,price);
                            items.store(reference);
                        }

                        if (cb_xxx_large.isChecked()) {
                            MyItems items = new MyItems(colors, MyConstants.mySize.xxxLarge, url,itemGender,itemClothes,  dateTime,price);
                            items.store(reference);
                        }
                    }
                });
            }
        });

        Log.e("MyItems", data.toString());
        Log.e("MyItems", data.size() + "");

    }


    public void removeSizeView(LinearLayout linearLayout) {
        linearLayout.removeAllViewsInLayout();
    }

    public void checkedChangeListener() {
        cbBlack.setOnCheckedChangeListener(this);
        cbYellow.setOnCheckedChangeListener(this);
        cbBlue.setOnCheckedChangeListener(this);
        cbMagenta.setOnCheckedChangeListener(this);
        cbGray.setOnCheckedChangeListener(this);
        cbGreen.setOnCheckedChangeListener(this);
        cbWhite.setOnCheckedChangeListener(this);
        cbRed.setOnCheckedChangeListener(this);
    }

    public void findViewById() {
        cbRed = findViewById(R.id.cbRed);
        cbBlack = findViewById(R.id.cbBlack);
        cbBlue = findViewById(R.id.cbBlue);
        cbMagenta = findViewById(R.id.cbMagenta);
        cbGray = findViewById(R.id.cbGray);
        cbYellow = findViewById(R.id.cbYellow);
        cbGreen = findViewById(R.id.cbGreen);
        cbWhite = findViewById(R.id.cbWhite);
        containerRed = findViewById(R.id.containerRed);
        containerYellow = findViewById(R.id.containerYellow);
        containerBlue = findViewById(R.id.containerBlue);
        containerGreen = findViewById(R.id.containerGreen);
        containerMagenta = findViewById(R.id.containerMagenta);
        containerWhite = findViewById(R.id.containerWhite);
        containerBlack = findViewById(R.id.containerBlack);
        containerGray = findViewById(R.id.containerGray);
        mainCont = findViewById(R.id.mainCont);
        btnContinue = findViewById(R.id.btnContinue);
        sp_clothes = findViewById(R.id.spClothes);
        sp_gender = findViewById(R.id.spGender);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 200 && data != null) {
            Uri imageUri = data.getData();
            imageView.setImageURI(imageUri);
            Toast.makeText(getApplicationContext(), imageUri.toString(), Toast.LENGTH_LONG).show();
        }
    }

}
