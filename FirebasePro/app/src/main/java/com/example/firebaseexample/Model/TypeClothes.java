package com.example.firebaseexample.Model;

public class TypeClothes {
    private int id;
    private String name;
    private int imge;

    public TypeClothes(int id, String name, int imge) {
        this.id = id;
        this.name = name;
        this.imge = imge;
    }

    public TypeClothes() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImge() {
        return imge;
    }

    public void setImge(int imge) {
        this.imge = imge;
    }
}
