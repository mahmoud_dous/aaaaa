package com.example.ui_project;

import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.example.ui_project.fragments.MessageFragment;
import com.example.ui_project.fragments.ShopCartFragment;
import com.example.ui_project.fragments.VendorFragment;
import com.example.ui_project.fragments.profile_fragment;

public class StorePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        getSupportFragmentManager().beginTransaction().add(R.id.main_frame, new profile_fragment()).commit();


        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.profile:
                                getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, new profile_fragment()).commit();
                                item.setChecked(true);
                                break;

                            case R.id.shopCart:
                                getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, new ShopCartFragment()).addToBackStack(null).commit();
                                item.setChecked(true);
                                break;

                            case R.id.message:
                                getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, new MessageFragment()).addToBackStack(null).commit();
                                item.setChecked(true);
                                break;

                            case R.id.vendor:
                                getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, new VendorFragment()).addToBackStack(null).commit();
                                item.setChecked(true);
                                break;

                        }
                        return false;
                    }
                });

    }
}
